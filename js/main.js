"use strict"


/**
 * This file is auto loaded by require JS.
 * Require JS has been provided for quick
 * startup, feel free to change this approach
 * as you see fit and add dependencies
 *
 * A List of button and text box ID's are
 * included below to speed up development
 * and reduce the need to scan through the
 * index file to acquire them.

	nameInput
	createCompanyBtn
	bankruptBtn
	addProductBtn
	floatRandomBtn
	encourageMergersBtn

 * Good luck
 *
 **/


requirejs.config({
    paths: {
        "jquery": "vendor/jquery"

    }
});
define(["jquery"], function () {

    var products = [
	    "HR",
	    "Research",
	    "Marketing",
	    "Risk Analysis",
	    "Training",
	    "Sales",
	    "Aquisitions",
	    "Charity work",
	    "Health Care",
	    "Prostitution",
	    "Money Laundering",
	    "Racketeering",
	    "Smuggling",
	    "Human Trafficking"
    ]
    var messagePanel;
    var worldElement = $('#world');

    var productCount = products.length

    var companies = [];
    var nameInputElement;

    function findRandomElement(array, remove) {
        var index = Math.floor((Math.random() * array.length))

        var rtn = array[index]

        if (remove) {
            array.splice(index, 1)
        }

        return rtn
    }


    var Company = function (name) {
        var self = this;

        self.companyName = name;
        self.products = [];
        self.isBankrupt = false;
        self.isFloated = false;
        self.isDisplayed = false;

        self.id = ('cmp' + Math.random()).replace('.', '')
    }

    Company.prototype.addToUi = function () {

        worldElement.html(
            worldElement.html() +
            '<div class="company" id="' + this.id + '"><span class="name">' + this.companyName + '</span>' +
                '<ul class="assets">' +
                '<ul>' +
            '</div>'
        );


        this.isDisplayed = true;
    }
    

    Company.prototype.ensureDisplayed = function () {
        if (!this.isDisplayed) {
            this.addToUi()
        }
    }

    Company.prototype.updateUi = function () {
        var assetLis = '<ul class="assets">';
        this.products.map(function (product) {
            assetLis = assetLis + '<li>' + product + '</li>'
        })
        assetLis = assetLis + '</li>';

        $('#' + this.id + '>ul ').html(assetLis)
        $('#' + this.id + ' .name').text(this.companyName)

    }

    Company.prototype.addProduct = function () {
        this.products.push(findRandomElement(products));
        this.updateUi();
    }

    Company.prototype.makeBankrupt = function () {
        this.isBankrupt = true;
        $('#' + this.id).addClass('bankrupt')
    }

    Company.prototype.float = function () {
        this.isFloated = true;
        $('#' + this.id).addClass('public')
    }

    function displayMessage(message) {
        messagePanel = messagePanel || $('#alertBox');
        messagePanel.text(message)
    }


    function performActions(predicate, action, functionName) {

        var eligibleCompanies = [];
        companies.map(function (company) {
            if (predicate(company)) {
                eligibleCompanies.push(company)
            }
        })

        if (eligibleCompanies.length == 0) {
            displayMessage("Cannot " + functionName + ". \nThere are no eligible companies");
            return;
        }

        var company = findRandomElement(eligibleCompanies)

        action(company);

    }

    function bankruptCompany() {

        performActions(
            function (company) {
                return !company.isBankrupt
            },
            function (company) {
                company.makeBankrupt()
            },

            "Bankrupt Company"
        )
    }

    function addProductToCompany() {

        performActions(
            function (company) {
                return !company.isBankrupt && !company.isFloated
            },
            function (company) {
                company.addProduct()
            },

            "Add product"
        )
    }

    function floatCompany() {

        performActions(
            function (company) {
                return !company.isBankrupt && !company.isFloated
            },
            function (company) {
                company.float()
            },

            "Float Company"
        )

    }

    function findCompaniesToMerge() {
        var eligibleCompanies = [];
        var rtn = []

        companies.map(function (company) {

            if (!company.isBankrupt && company.isFloated) {
                eligibleCompanies.push(company)
            }
        })

        if (eligibleCompanies.length < 2) {
            return []
        }

        rtn.push(findRandomElement(eligibleCompanies, true))
        rtn.push(findRandomElement(eligibleCompanies, true))

        return rtn;
    }

    function mergeCompanies() {
        var companiesToMerge = findCompaniesToMerge()

        if (companiesToMerge.length != 2) {
            displayMessage("Cannot Encourage Mergers. \nThere are not enough eligible companies")
        }

        var biggerCompany = companiesToMerge[0]
        var smallerCompany = companiesToMerge[1]

        if (smallerCompany.products.length > biggerCompany.products.length) {
            biggerCompany = companiesToMerge[1];
            smallerCompany = companiesToMerge[0];
        }

        biggerCompany.products = biggerCompany.products.concat(smallerCompany.products);
        biggerCompany.companyName = biggerCompany.companyName + ' ' + smallerCompany.companyName;
        biggerCompany.updateUi();

        removeCompany(smallerCompany)
    }

    function removeItem(array, item) {

        for (var i = 0; i < array.length;i++){
            if (array[i] == item) {
                array.splice(i, 1)
                return;
            }
        }
    }

    function removeCompany(smallerCompany) {
        $('#' + smallerCompany.id).remove()
        removeItem(companies, smallerCompany);
        smallerCompany = null;
    }

    function addCompany() {
        nameInputElement = nameInputElement || $('#nameInput')
        var coName = nameInputElement.val()

        if (coName == "") {
            displayMessage("Please add a name for the company")
            return;
        }

        var company = new Company(coName);
        company.addToUi();

        companies.push(company)
        nameInputElement.val("");
    }

    $(function () {

        $('#createCompanyBtn').click(addCompany);
        $('#bankruptBtn').click(bankruptCompany);
        $('#addProductBtn').click(addProductToCompany);
        $('#floatRandomBtn').click(floatCompany);
        $('#encourageMergersBtn').click(mergeCompanies);

    })
});
